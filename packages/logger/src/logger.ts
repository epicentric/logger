import {ContextType, LoggerChild, LogLevel, LogObject} from './logger-child';
import {Writable} from 'readable-stream';

export interface LoggerOutput
{
    child: LoggerChild;
    log: LogObject;
}

export class Logger extends LoggerChild
{
    private outputs: Writable[] = [];
    
    public constructor(context: ContextType = {}, maxHistory: number = 100)
    {
        super(null, context, maxHistory);
    }

    public async commit(child: LoggerChild, log: LogObject): Promise<void>
    {
        for (const output of this.outputs)
        {
            const outputToWrite: LoggerOutput = { child, log };
            
            output.write(outputToWrite);
        }
        
        if (LogLevel.Fatal === log.level)
        {
            await this.close();
            
            if (typeof process !== 'undefined')
                process.exit(1);
        }
    }
    
    public close(): Promise<any>
    {
        return Promise.all(this.outputs.map(stream => {
            return new Promise((resolve, reject) => {
                stream.end(resolve);
            });
        }));
    }
    
    public addOutput(output: Writable): void
    {
        this.outputs.push(output);
    }
}