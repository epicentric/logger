import CircularBuffer from 'mnemonist/circular-buffer';
import {Logger} from './logger';

export interface ContextType
{
    [key: string]: any;
}

export enum LogLevel
{
    Trace   = 'trace',
    Debug   = 'debug',
    Info    = 'info',
    Warn    = 'warn',
    Error   = 'error',
    Fatal   = 'fatal'
}

export interface LogObject extends ContextType
{
    timestamp: number;
    level: LogLevel;
    message?: string;
}

export class LoggerChild
{    
    public history: CircularBuffer<LogObject>;
    
    public constructor(public parent: LoggerChild|null, public context: ContextType, maxHistory: number)
    {
        this.history = new CircularBuffer(Array, maxHistory);
    }

    public trace(message: string): void
    public trace(details: ContextType): void
    public trace(details: ContextType, message: string): void
    public trace(detailsOrMessage: ContextType|string, message?: string): void
    {
        this.onLog(LogLevel.Trace, detailsOrMessage, message);
    }

    public debug(message: string): void
    public debug(details: ContextType): void
    public debug(details: ContextType, message: string): void
    public debug(detailsOrMessage: ContextType|string, message?: string): void
    {
        this.onLog(LogLevel.Debug, detailsOrMessage, message);
    }

    public info(message: string): void
    public info(details: ContextType): void
    public info(details: ContextType, message: string): void
    public info(detailsOrMessage: ContextType|string, message?: string): void
    {
        this.onLog(LogLevel.Info, detailsOrMessage, message);
    }

    public log(message: string): void
    public log(details: ContextType): void
    public log(details: ContextType, message: string): void
    public log(detailsOrMessage: ContextType|string, message?: string): void
    {
        this.onLog(LogLevel.Info, detailsOrMessage, message);
    }

    public warn(message: string): void
    public warn(details: ContextType): void
    public warn(details: ContextType, message: string): void
    public warn(detailsOrMessage: ContextType|string, message?: string): void
    {
        this.onLog(LogLevel.Warn, detailsOrMessage, message);
    }

    public error(message: string): void
    public error(details: ContextType): void
    public error(details: ContextType, message: string): void
    public error(detailsOrMessage: ContextType|string, message?: string): void
    {
        this.onLog(LogLevel.Error, detailsOrMessage, message);
    }


    public fatal(message: string): void
    public fatal(details: ContextType): void
    public fatal(details: ContextType, message: string): void
    public fatal(detailsOrMessage: ContextType|string, message?: string): void
    {
        this.onLog(LogLevel.Fatal, detailsOrMessage, message);
    }
    
    private onLog(level: LogLevel, detailsOrMessage: ContextType|string, message?: string)
    {
        let actualMessage: string|undefined;
        let actualDetails: ContextType;
        
        if (typeof detailsOrMessage === 'string')
        {
            actualMessage = detailsOrMessage;
            actualDetails = {};
        }
        else
        {
            actualMessage = message;
            actualDetails = detailsOrMessage;
        }
        
        this.logObject({
            timestamp: Date.now(),
            level,
            message: actualMessage,
            ...actualDetails
        });
    }
    
    protected logObject(object: LogObject)
    {
        const contexts: ContextType[] = [object, this.context];
        const finalLogObject: LogObject = {} as LogObject;
        
        let currentChild: LoggerChild = this;
        
        while (currentChild.parent)
        {
            currentChild.history.push(finalLogObject);
            currentChild = this.parent!;
            contexts.push(currentChild.context);
        }
        
        Object.assign(finalLogObject, ...contexts.reverse());
        
        (currentChild as Logger).commit(this, finalLogObject);
    }
    
    public child(context: ContextType = {}, maxHistory: number = this.history.capacity): LoggerChild
    {
        return new LoggerChild(this, context, maxHistory);
    }
}