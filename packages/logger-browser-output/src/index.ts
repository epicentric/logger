import {Writable} from 'readable-stream';
import {LoggerOutput, LogLevel} from '@epicentric/logger';
import {format} from 'date-fns';

const levelMethods = new Map<LogLevel, Function>([
    [LogLevel.Fatal,    console.error],
    [LogLevel.Error,    console.error],
    [LogLevel.Warn,     console.warn],
    [LogLevel.Info,     console.info],
    [LogLevel.Debug,    console.debug],
    [LogLevel.Trace,    console.trace]
]);

export const customFormatterSymbol = Symbol.for('browserOutputStream.customFormatter');

export class BrowserOutputStream extends Writable
{
    public constructor()
    {
        super({ objectMode: true });
    }

    public _write(chunk: LoggerOutput, encoding: string, callback: (error?: (Error | null)) => void): void
    {
        try
        {
            const {level, message, timestamp, [customFormatterSymbol as any]: customFormatter, ...details} = chunk.log;
            
            const timestampString = format(timestamp, '[HH:mm:ss.SSS]');
            let args: any[];
                        
            if (customFormatter)
            {
                args = customFormatter(chunk, timestampString);
            }
            else
            {
                args = [timestampString];
                
                if (message)
                    args.push(message);
                
                if (Object.keys(details).length)
                    args.push(details);
            }
            
            const method = levelMethods.get(level);
            
            if (method)
                method(...args);
            
            callback();
        }
        catch (err)
        {
            callback(err);
        }        
    }
}