import util from 'util';
import os from 'os';
import chalk from 'chalk';
import {cloneDeepWith} from 'lodash-es';
import {Transform, TransformCallback} from 'stream';
import {fromBinaryId, isBinaryId} from '@epicentric/binary-id';
import {LoggerOutput, LogLevel} from '@epicentric/logger';
import {format} from 'date-fns';

const utilOptions: util.InspectOptions =  {
    depth: 4,
    colors: true,
    breakLength: Number.POSITIVE_INFINITY
};

const levelStrings = new Map<LogLevel, string>([
    [LogLevel.Fatal,    chalk.bgRed('fatal')],
    [LogLevel.Error,    chalk.red('error')],
    [LogLevel.Warn,     chalk.yellow('warn')],
    [LogLevel.Info,     chalk.green('info')],
    [LogLevel.Debug,    chalk.blue('debug')],
    [LogLevel.Trace,    chalk.grey('trace')]
]);

// Taken from: https://gist.github.com/tauzen/3d18825ae41ff3fc8981
function byteToHexString(uint8arr: Uint8Array)
{
    let hexStr = '';

    for (let i = 0; i < uint8arr.length; i++)
    {
        let hex = (uint8arr[i] & 0xff).toString(16);
        hex = (hex.length === 1) ? '0' + hex : hex;
        hexStr += hex;
    }

    return hexStr;
}

function formatData(data: object): object
{
    return cloneDeepWith(data, (value, key) => {
        if (value instanceof Uint8Array)
        {
            return `Uint8Array(${byteToHexString(value)})`;
        }
        else if (isBinaryId(value))
        {
            return `BinaryId(${fromBinaryId(value)})`;
        }

        return undefined;
    });
}

export class LoggerNodePrettifier extends Transform
{
    public constructor()
    {
        super({ objectMode: true });
    }

    public _transform(chunk: LoggerOutput, encoding: string, callback: TransformCallback): void
    {
        try
        {
            let {timestamp, level, message = '', ...details} = chunk.log;

            let errorProps = null;
            if (details instanceof Error)
            {
                errorProps = {
                    type: 'Error',
                    stack: details.stack
                }
            }

            const logData = Object.assign(details, errorProps);

            let dataOutput = util.inspect(formatData(logData), utilOptions);
            dataOutput = dataOutput
                .replace(/(?<!\\)'/g, '')
                .replace(/\\'/g, '\'');

            const output = `${format(timestamp, 'yyyy-MM-dd HH:mm:ss.SSSS')} - ${levelStrings.get(level)}: ${chalk.bold(message)} ${dataOutput}${os.EOL}`;

            callback(undefined, output);
        }
        catch (err)
        {
            callback(err);
        }
    }
}