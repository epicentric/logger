import {BaseSentryWriteableStream} from './base-sentry-writable-stream';
import {NodeClient, NodeOptions} from '@sentry/node';
import {LogLevel} from '@epicentric/logger';

const disabledIntegrations = [
    'Console',
    'OnUncaughtException',
    'OnUnhandledRejection',
];

export class NodeSentryWritableStream extends BaseSentryWriteableStream
{
    public constructor(levels: LogLevel[], options: NodeOptions)
    {
        const client = new NodeClient({
            ...options,
            integrations: integrations => {
                return integrations.filter(integration => !disabledIntegrations.includes(integration.name));
            }
        }); 
        
        super(levels, client);
    }
}