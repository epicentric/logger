import {Writable} from 'readable-stream';
import {Severity} from '@sentry/types';
import {LoggerOutput, LogLevel} from '@epicentric/logger';
import {getCurrentHub} from '@sentry/hub';
import {BaseClient} from '@sentry/core';

const sentryLevelMap = new Map<LogLevel, Severity>([
    [LogLevel.Fatal,    Severity.Fatal],
    [LogLevel.Error,    Severity.Error],
    [LogLevel.Warn,     Severity.Warning],
    [LogLevel.Info,     Severity.Info],
    [LogLevel.Debug,    Severity.Debug],
    [LogLevel.Trace,    Severity.Debug]
]);

export abstract class BaseSentryWriteableStream extends Writable
{
    protected constructor(
        public levels: LogLevel[],
        protected readonly client: BaseClient<any, any>
    )
    {
        super({
            highWaterMark: 2,
            decodeStrings: false,
            objectMode: true
        });
    }

    public async _write(chunk: LoggerOutput, encoding: string, callback: (error?: (Error | null)) => void): Promise<void>
    {
        try
        {
            let {timestamp, level, message = '', ...details} = chunk.log;
            
            if (this.levels.includes(level))
            {
                let error: Error|null = null;
                if (details instanceof Error)
                {
                    error = details;
                    details = {};
                }
                else if (details.err instanceof Error)
                {
                    error = details.err;
                    delete details.err;
                }
    
                let promise: Promise<void>;
                getCurrentHub().withScope(scope => {
                    promise = (async () => {
                        if (!(details instanceof Error))
                        {
                            for (const [key, value] of Object.entries(details))
                            {
                                if ('err' === key)
                                    continue;
    
                                scope!.setExtra(key, value);
                            }
                        }
    
                        const sentryLevel = sentryLevelMap.get(level) || Severity.Info;
                        if (error)
                        {
                            scope.setLevel(sentryLevel);
                            await this.client.captureException(error, details, scope);
                        }
                        else if (message)
                        {
                            await this.client.captureMessage(message, sentryLevel, details, scope);
                        }
                    })();
                });
                
                // This is needed so we can close the stream and know that all logs were sent to sentry
                await promise!;
            }

            callback();
        }
        catch (err)
        {
            callback(err);
        }
    }
}