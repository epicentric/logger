import {BaseSentryWriteableStream} from './base-sentry-writable-stream';
import {LogLevel} from '@epicentric/logger';
import {BrowserClient, BrowserOptions, Integrations} from '@sentry/browser';
import {ReportingObserver} from '@sentry/integrations';

export class BrowserSentryWritableStream extends BaseSentryWriteableStream
{
    public constructor(levels: LogLevel[], options: BrowserOptions)
    {
        const client = new BrowserClient({
            ...options,
            integrations: [
                new Integrations.Breadcrumbs({ console: false }),
                new ReportingObserver()
            ]
        });

        super(levels, client);
    }
}